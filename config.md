<!--
Add here global page variables to use throughout your website.
-->

@def author = "Navi"

@def mintoclevel = 2

@def ignore = ["node_modules/",]
+++
generate_rss = true
website_title = "Navi Blog"
website_descr = "Personal Blog about quantitative methods for decision making, Tech and slice of life"
website_url   = "https://indymnv.dev"
rss_full_content = true
+++

\newcommand{\R}{\mathbb R} 
\newcommand{\scal}[1]{\langle #1 \rangle}
