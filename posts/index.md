
# Posts

* 2023-09-02 [Brief ideas about language learning. What is worth learning?](/posts/007_elderly_fall_1/)
* 2023-08-10 [Creating your own blog with Franklin.jl](/posts/006_build_blog/)
* 2023-07-04 [Set up NeoVim + Tmux for a Data Science Workflow with Julia](/posts/004_nvim/)
* 2023-03-02 [Using Evotrees.jl for time series prediction](/posts/003_publish/)
* 2022-12-15 [Using Python with Selenium and Pandas for web scrapping](/posts/002_publish/)
* 2022-11-23 [EDA & Clustering - World Happiness Score](/posts/005_happines/)
* 2022-10-05 [The Purpose of this Blog](/posts/001_publish/)
