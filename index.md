@def title = "Navi's Blog"
@def tags = ["syntax", "code"]
+++
sitemap_changefreq = "yearly"
sitemap_priority = 0.8
+++

> Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less.
>
> --- Marie Curie

# ¡Hola! Welcome to my blog

~~~
    <img src="/assets/profile_picture.jpeg" height="300" class="main-picture" >
    <p>
    <p>
~~~

My name is Indy Navarro (a.k.a Navi), a Chilean industrial engineer living in Tokyo, Japan. I have previous academic experience studying optimization in emergency systems, and in the industry, I have worked in the energy and food sectors as a data analyst/scientist and business intelligence engineer.

This space is used to share knowledge about the world of quantitative methods for decision-making, technology, and reflections on life. I have used different tools, preferably open source, but currently, I primarily use Julia and Python.

To work with me or stay in contact, you can follow me on [Mastodon](https://fosstodon.org/@mikelech) or write an~~~ <a href="mailto:&#105;&#110;&#100;&#121;&#046;&#109;&#110;&#118;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;"> email</a>~~~. The code for my projects can be found on [Github](https://github.com/indymnv) or to subscribe to my blog, use the [RSS's feed](https://www.indymnv.dev/feed.xml)    
